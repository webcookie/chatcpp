#include "Client.h"

Client::Client(QTcpSocket *socket) {
    this->socket_client = socket;
    this->expectedDataSize = 0;
    this->clientInfo = 0;

    //Connect this client socket signals
    connect(this->socket_client, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(this->socket_client, SIGNAL(disconnected()), this, SLOT(disconnected()));
}

void Client::disconnect(){
    this->socket_client->disconnectFromHost();
}

void Client::setClientInfo(ClientInfo *clientInfo){
    this->clientInfo = clientInfo;
}

ClientInfo *Client::getClientInfo(){
    return this->clientInfo;
}


void Client::readyRead(){
    QDataStream in(this->socket_client);

    //Check if it is a new data and try to get its expected size
    if (this->expectedDataSize == 0)
    {
        if (this->socket_client->bytesAvailable() < (int)sizeof(quint16)){
            //Buffer doesn't contain expected size yet. Continue waiting.
            return;
        }
        in >> this->expectedDataSize; //Store expected data size
    }

    //Now that we know expected data size, let's wait until we got all data
    if (this->socket_client->bytesAvailable() < this->expectedDataSize){
        return;
    }

    //Read data
    QString data;
    in >> data;

    //We finally received whole data. We can reset expected data size.
    this->expectedDataSize = 0;

    //Notice listeners
    emit dataReceived(this, data);
    readyRead();
}

void Client::sendData(Command *command){
    sendData(command->getFormattedCommand());
}

void Client::sendData(QString data){
    QByteArray paquet;
    QDataStream out(&paquet, QIODevice::WriteOnly);

    //Leave space for paquet size
    out << (quint16) 0;
    //Append command
    out << data;
    //Move cursor back to index 0.
    out.device()->seek(0);
    //Write size
    out << (quint16) (paquet.size() - sizeof(quint16));
    //Send packet
    this->socket_client->write(paquet);
}

void Client::disconnected(){
    //This client has disconnected. Notice listeners
    emit clientDisconnected(this);
}
