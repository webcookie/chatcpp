#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QtGlobal>
#include <QDataStream>
#include <QByteArray>
#include <QThread>

#include "ClientInfo.h"
#include "Command.hpp"

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QTcpSocket *socket);
    void disconnect();
    void sendData(QString data);
    void sendData(Command *command);
    void setClientInfo(ClientInfo *);
    ClientInfo *getClientInfo();

signals:
    void clientDisconnected(Client *client);
    void dataReceived(Client *sender, QString data);

private slots:
    void readyRead();
    void disconnected();

private:
    QTcpSocket *socket_client;
    ClientInfo *clientInfo;
    quint16 expectedDataSize;

};

#endif // CLIENT_H
