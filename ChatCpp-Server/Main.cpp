#include <QApplication>
#include <iostream>

#include "GUI.h"
#include "Utils.h"

int main(int argc, char* argv[]){
    QApplication *app = new QApplication(argc, argv);

    GUI *gui = new GUI();
    gui->setVisible(true);

    int ret = app->exec();

    qDebug() << "Unregister server on API";
    Utils::unregisterServerIP();

    return ret;
}
