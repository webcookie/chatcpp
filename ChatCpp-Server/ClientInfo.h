#ifndef CLIENTINFO_H
#define CLIENTINFO_H

#include <QString>

class ClientInfo {
public:
    ClientInfo(QString username, QString computerName){this->computerName = computerName, this->username = username;}
    QString getUsername(){return this->username;}
    QString getComputerName(){return this->computerName;}
private:
    QString username;
    QString computerName;
};

#endif // CLIENTINFO_H
