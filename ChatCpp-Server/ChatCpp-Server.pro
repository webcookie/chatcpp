QT += widgets
QT += network
QT += core

TEMPLATE = app

SOURCES += \
    Main.cpp \
    Server.cpp \
    Client.cpp \
    ServerController.cpp \
    GUI.cpp

HEADERS += \
    Server.h \
    Client.h \
    ServerController.h \
    ClientInfo.h \
    GUI.h \
    Q_DebugStream.h \
    Utils.h

FORMS += \
    GUI.ui

INCLUDEPATH += $$PWD/../ChatCpp-Commands
DEPENDPATH += $$PWD/../ChatCpp-Commands
