#include "ServerController.h"

ServerController::ServerController(Server *server)
{
    this->server = server;

    connect(this->server, SIGNAL(receivedDataFromClient(Client*,QString)), this, SLOT(receivedDataFromClient(Client*,QString)));
    connect(this->server, SIGNAL(clientConnected(Client*)), this, SLOT(clientConnected(Client*)));
    connect(this->server, SIGNAL(clientDisconnected(Client*)), this, SLOT(clientDisconnected(Client*)));
}

void ServerController::clientConnected(Client *client){
    //Whois new client
    client->sendData(new CommandGWhoisClient());
}

void ServerController::clientDisconnected(Client *client){
    //Tell everyone client has disconnected
    if(client->getClientInfo()){
        emit userDisconnected(client->getClientInfo()->getUsername());
        sendCommandToAllClients(new CommandClientDisconnected(client->getClientInfo()->getUsername()));        
    }    
}

void ServerController::receivedDataFromClient(Client * client, QString data){
    if(data.startsWith(COMMAND_GUSERS_LIST)){
        QStringList clientsName = QStringList();
        QList<Client*> clients = this->server->getClientsList();
        //Read all clients username
        for(int i = 0; i < clients.size(); i++){
            clientsName.append(clients.at(i)->getClientInfo()->getUsername());
        }
        client->sendData(new CommandUsersList(clientsName));
    }else if(data.startsWith(COMMAND_MESSAGE)){
        //Check if client is muted
        if(client->getClientInfo() && this->mutedUsers.contains(client->getClientInfo()->getUsername())){
            //Tell user he is muted.
            client->sendData(new CommandMessage("Console", "### SHHHHHH keep quiet. ###"));
        }else{
            //Transfer chat message to everyone
            CommandMessage *commandMessage = CommandMessage::parseCommand(data);
            sendCommandToAllClients(commandMessage);
            emit onMessageCommand(commandMessage);
        }
    }else if(data.startsWith(COMMAND_WHOIS_CLIENT)){
        CommandWhoisClient *command = CommandWhoisClient::parseCommand(data);

        //Check if username is already in use
        bool usernameInUse = false;
        for(int i = 0; i < this->server->getClientsList().size(); i++){
            if(this->server->getClientsList().at(i)->getClientInfo() && this->server->getClientsList().at(i)->getClientInfo()->getUsername() == command->getUsername()){
                usernameInUse = true;
                break;
            }
        }
        if(!usernameInUse){
            //Set Client Info
            client->setClientInfo(new ClientInfo(command->getUsername(), command->getComputerName()));
            //Accept the client
            client->sendData(new CommandConnectionAccept());
            //Emit Signal
            emit userAuthenticated(client->getClientInfo()->getUsername());
            //Notice everyone someone has connected
            sendCommandToAllClients(new CommandNewClientConnected(command->getUsername()));            
        }else{
            client->sendData(new CommandConnectionRefused("Username already in use."));
            client->disconnect();
        }
    }else{
        qDebug() << "Server received an unknown command : " << data.split(";").at(0);
    }
}

bool ServerController::toggleMuteUser(Client *client){
    if(client->getClientInfo()){
        bool mute = false;
        QString username = client->getClientInfo()->getUsername();
        if(!mutedUsers.contains(username)){
            mutedUsers.append(username);
            mute = true;
        }else{
            mutedUsers.removeOne(username);
        }
        client->sendData(new CommandMute(client->getClientInfo()->getUsername(), mute));
        return mute;
    }
    return false;
}

void ServerController::sendCommandToAllClients(Command *command){
    QList<Client*> clients = this->server->getClientsList();

    for(int i = 0; i < clients.size(); i++){
        clients.at(i)->sendData(command);
    }
}
