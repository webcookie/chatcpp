#include "GUI.h"
#include "ui_GUI.h"

GUI::GUI() : ui(new Ui::GUI)
{
    ui->setupUi(this);

    this->setWindowTitle("Server - Panneau de contrôle");

    //Redirect Console output to QTextEdit
    new Q_DebugStream(std::cout, ui->txt_console_messages);
    //Redirect qDebug() output to QTextEdit
    Q_DebugStream::registerQDebugMessageHandler();

    //Start Server
    server = new Server(Utils::port);
    serverController = new ServerController(server);

    //Connect signals
    connect(this->serverController, SIGNAL(userAuthenticated(QString)), this, SLOT(onUserAuthenticated(QString)));
    connect(this->serverController, SIGNAL(userDisconnected(QString)), this, SLOT(onUserDisconnected(QString)));
    connect(this->serverController, SIGNAL(onMessageCommand(CommandMessage*)), this, SLOT(onMessageCommand(CommandMessage*)));
}

GUI::~GUI()
{
    delete ui;
}

void GUI::onUserAuthenticated(QString username){
    ui->list_clients->addItem(username);
}

void GUI::onUserDisconnected(QString username){
    for(int i = 0; i < ui->list_clients->count(); i++){
        if(ui->list_clients->item(i)->text() == username){
            delete ui->list_clients->takeItem(i);
            break;
        }
    }
}

void GUI::on_txt_console_command_returnPressed()
{
    if(ui->txt_console_command->text().length() > 3){
        for(int i = 0; i < this->server->getClientsList().size(); i++){
            this->server->getClientsList().at(i)->sendData(ui->txt_console_command->text());
        }
        ui->txt_console_command->clear();
    }
}

void GUI::on_bt_disconnect_client_clicked()
{
    if(ui->list_clients->selectedItems().length() >= 1){
        Client *client = this->server->getClientForUsername(ui->list_clients->selectedItems().at(0)->text());

        if(client){
            client->disconnect();
        }
    }
}

void GUI::on_bt_merrychristmas_client_clicked()
{
    if(ui->list_clients->selectedItems().length() >= 1){
        Client *client = this->server->getClientForUsername(ui->list_clients->selectedItems().at(0)->text());

        if(client){
            client->sendData(new CommandChristmasSong());
        }
    }
}

void GUI::on_bt_play_youtube_client_clicked()
{
    if(ui->list_clients->selectedItems().length() >= 1){
        Client *client = this->server->getClientForUsername(ui->list_clients->selectedItems().at(0)->text());

        if(client){
            bool input_ok_pressed;
            QString youtubeURL = QInputDialog::getText(this, "Youtube URL", "Youtube URL :", QLineEdit::Normal,"", &input_ok_pressed);
            if(input_ok_pressed){
               client->sendData(new CommandYoutubeMP3(youtubeURL));
            }
        }
    }
}

void GUI::on_bt_send_msg_to_client_clicked()
{
    if(ui->list_clients->selectedItems().length() >= 1){
        Client *client = this->server->getClientForUsername(ui->list_clients->selectedItems().at(0)->text());

        if(client){
            bool input_ok_pressed;
            QString message = QInputDialog::getText(this, "Type a message", "Message :", QLineEdit::Normal,"", &input_ok_pressed);
            if(input_ok_pressed){
               client->sendData(new CommandMessage("Console", message));
            }
        }
    }
}

void GUI::on_bt_send_as_client_clicked()
{
    if(ui->list_clients->selectedItems().length() >= 1){

        bool input_ok_pressed;
        QString message = QInputDialog::getText(this, "Type a message", "Message :", QLineEdit::Normal,"", &input_ok_pressed);
        if(input_ok_pressed){
            this->server->broadcastCommand(new CommandMessage(ui->list_clients->selectedItems().at(0)->text(), message));
        }

    }
}

void GUI::on_bt_mute_client_clicked()
{
    if(ui->list_clients->selectedItems().length() >= 1){
        Client *client = this->server->getClientForUsername(ui->list_clients->selectedItems().at(0)->text());

        if(client){
            bool mute = this->serverController->toggleMuteUser(client);
            if(mute){
                QMessageBox::information(this, "Mute", ui->list_clients->selectedItems().at(0)->text() + " has been <b>muted</b>.", QMessageBox::Ok);
            }else{
                QMessageBox::information(this, "Mute", ui->list_clients->selectedItems().at(0)->text() + " has been <b>unmuted</b>.", QMessageBox::Ok);
            }
        }
    }
}

void GUI::onMessageCommand(CommandMessage *command){
    ui->txt_chat->append(command->getSender() + ": " + command->getMessage());
}

void GUI::on_txt_send_message_chat_returnPressed()
{
    if(ui->txt_send_message_chat->text().length() > 0){
        this->server->broadcastCommand(new CommandMessage("Console", ui->txt_send_message_chat->text()));
        ui->txt_chat->append("Console: " + ui->txt_send_message_chat->text());
        ui->txt_send_message_chat->clear();
    }
}
