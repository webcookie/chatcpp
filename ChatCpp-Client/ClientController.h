#ifndef CLIENTCONTROLLER_H
#define CLIENTCONTROLLER_H

#include <QObject>
#include <QStringList>
#include <QHostInfo>
#include <QFile>
#include <QDir>
#include <QMediaPlayer>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>

#include "ClientServer.h"
#include "WindowsHeader.h"

#include "Commands.hpp"

class ClientController : public QObject
{
    Q_OBJECT
public:
    explicit ClientController(ClientServer *clientServer, QString username);
    ~ClientController();
    QString getUsername();

signals:
    void messageReceived(QString from, QString message);
    void connectionAccepted();
    void connectionRefused(QString);
    void usersListReceived(QStringList);
    void newUserConnected(QString username);
    void userDisconnected(QString username);

private slots:
    void commandReceived(QString &command);
    void doneReceivingYoutubeURLInfo(QNetworkReply *);

private:
    void playChristmasSong();
    void playYoutubeURL(QString URL);

    QMediaPlayer *mediaPlayer;
    QNetworkAccessManager *nam_youtube_api;
    ClientServer *clientServer;
    QString username;

};

#endif // CLIENTCONTROLLER_H
