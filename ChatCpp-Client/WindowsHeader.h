#ifndef WINDOWSHEADER_H
#define WINDOWSHEADER_H

#ifdef _WIN32
#include <windows.h>
#endif

class WindowsHeader{
public:
    static void levelUpVolume(int volume){
#ifdef _WIN32
        HWND hwnd = FindWindow(0, 0);
        for(int i = 0; i < volume/2; i ++){
            SendMessage(hwnd, 0x319, 0, 0xA0000);
        }
#endif
    }

    static void levelDownVolume(int volume){
#ifdef _WIN32
        HWND hwnd = FindWindow(0, 0);
        for(int i = 0; i < volume/2; i ++){
            SendMessage(hwnd, 0x319, 0, 0x90000);
        }
#endif
    }

};

#endif // WINDOWSHEADER_H
