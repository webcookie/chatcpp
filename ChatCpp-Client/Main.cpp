#include <QApplication>

#include "GUI_Login.h"


int main(int argc, char* argv[]){
    QApplication *app = new QApplication(argc, argv);

    GUI_Login *gui_login = new GUI_Login();
    gui_login->setVisible(true);

    return app->exec();
}

