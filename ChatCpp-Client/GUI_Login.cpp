#include "GUI_Login.h"
#include "ui_GUI_Login.h"

GUI_Login::GUI_Login() : ui(new Ui::GUI_Login)
{
    ui->setupUi(this);
    //Initialize variables
    this->gui_chat = 0;
    this->lbl_status = new QLabel("");
    //Status bar label
    ui->statusBar->addWidget(this->lbl_status);

    this->setWindowTitle("Chat - Connexion");
}

GUI_Login::~GUI_Login()
{
    delete ui;
}

void GUI_Login::on_bt_connect_clicked()
{
    if(ui->txt_username->text().length() > 0 && ui->txt_ip->text().length() > 0){
        clientServer = new ClientServer();
        clientController = new ClientController(clientServer, ui->txt_username->text());

        connect(clientServer, SIGNAL(connectedToServer()), this, SLOT(connectedToServer()));
        connect(clientServer, SIGNAL(disconnectedFromServer()), this, SLOT(disconnectedFromServer()));
        connect(clientServer, SIGNAL(serverError(QString)), this, SLOT(serverError(QString)));
        connect(clientController, SIGNAL(connectionAccepted()), this, SLOT(connectionAccepted()));
        connect(clientController, SIGNAL(connectionRefused(QString)), this, SLOT(connectionRefused(QString)));

        this->lbl_status->setText("Connecting...");
        ui->bt_connect->setEnabled(false);
        clientServer->connectToHost(ui->txt_ip->text(), 2048);
    }else {
        this->lbl_status->setText("IP ou nom d'utilisateur manquant.");
    }
}

void GUI_Login::connectedToServer(){
    this->lbl_status->setText("Authenticating...");
}

void GUI_Login::connectionAccepted(){
    gui_chat = new GUI_Chat(this->clientServer, this->clientController);
    gui_chat->setVisible(true);
    this->setVisible(false);
    this->lbl_status->setText("");
    ui->bt_connect->setEnabled(true);
}

void GUI_Login::connectionRefused(QString error){
    this->lbl_status->setText("Error : " + error);
    ui->bt_connect->setEnabled(true);
    disconnectedFromServer();
}

void GUI_Login::disconnectedFromServer(){
    ui->bt_connect->setEnabled(true);

    if(gui_chat){
        gui_chat->setVisible(false);
        gui_chat->close();
        gui_chat->deleteLater();
        gui_chat = 0;
    }
    this->setVisible(true);

    clientServer->deleteLater();
    clientController->deleteLater();
    clientServer = 0;
    clientController = 0;
}

void GUI_Login::serverError(QString error){
    if(!this->lbl_status->text().startsWith("Error")){
        this->lbl_status->setText("Error : " + error);
    }
    disconnectedFromServer();
}

void GUI_Login::on_bt_servers_list_clicked()
{
    DialogServerList *dialogServerList = new DialogServerList();
    QString selectedIP = dialogServerList->showAndWait();

    if(selectedIP.length() > 0){
        ui->txt_ip->setText(selectedIP);
    }
}
