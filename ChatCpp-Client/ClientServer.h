#ifndef CLIENTSERVER_H
#define CLIENTSERVER_H

#include <QObject>
#include <QTcpSocket>
#include <QByteArray>
#include <QDebug>
#include <QString>
#include <QtGlobal>
#include <QTime>
#include <QDebug>

#include "Command.hpp"

class ClientServer : public QObject{
    Q_OBJECT
public:
    ClientServer();
    void connectToHost(QString IP, qint16 port);
    void sendCommand(Command* command);
    void disconnect();

signals:
    void connectedToServer();
    void disconnectedFromServer();
    void commandReceived(QString &command);
    void serverError(QString);

private slots:
    void readyRead();
    void error(QAbstractSocket::SocketError);

private:
    QTcpSocket *socket;
    qint16 expectedDataSize;
};

#endif // CLIENTSERVER_H
