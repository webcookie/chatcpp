﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChatCppAPI_ASP
{
    public partial class Default : System.Web.UI.Page
    {
        static FileOperationThread fileOperationThread = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (fileOperationThread == null)
            {
                fileOperationThread = new FileOperationThread(Server.MapPath("~/IPs.txt"));
            }

            String task = Request["task"];
            String localIP = Request["localIP"];
            String externalIP = Request["externalIP"];
            String port = Request["port"];
            String pcName = Request["pcName"];

            //Get task doesn't require any other arguments
            if (task != null)
            {
                task = task.ToLower();

                if (task == "get")
                {
                    String IPs = fileOperationThread.getAllServerIPs();
                    lbl_serverReply.Text = IPs;
                    Response.Write("IPs : " + IPs);
                    return;
                }
            }

            if (task != null && localIP != null && externalIP != null && port != null && pcName != null && task.Length > 0 && localIP.Length > 0 && externalIP.Length > 0 && port.Length > 0 && pcName.Length > 0) 
            {
                task = task.ToLower();
                if (task == "add" || task == "delete")
                {
                    IPAddress useless;
                    bool validIP = IPAddress.TryParse(localIP, out useless);
                    bool validIP2 = IPAddress.TryParse(externalIP, out useless);

                    bool validPort = true;
                    try
                    {
                        Convert.ToInt32(port);
                    }
                    catch (Exception)
                    {
                        validPort = false;
                    }

                    if (validIP && validIP2 && validPort)
                    {
                        if (task == "add")
                        {
                            fileOperationThread.addServerIP(externalIP + ";" + localIP + ";" + port + ";" + pcName);
                            this.lbl_serverReply.Text = "IP Added.";
                            Response.Write("Done");
                        }
                        else if (task == "delete")
                        {
                            fileOperationThread.removeServerIP(externalIP + ";" + localIP + ";" + port + ";" + pcName);
                            this.lbl_serverReply.Text = "IP Deleted.";
                            Response.Write("Done");
                        }
                        else
                        {
                            //Invalid parameter task
                            this.lbl_serverReply.Text = "Invalid parameter task in URL.";
                        }
                    }
                    else
                    {
                        this.lbl_serverReply.Text = "Invalid IP Address or port.";
                    }                    
                }
            }
            else
            {
                //Missing parameter
                this.lbl_serverReply.Text = "Missing parameters in URL.";
            }
        }
    }
}