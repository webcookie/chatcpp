﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatCppAPI_ASP
{
    public class Task
    {
        private String text;
        private bool add;
        private bool done;
        private String output;

        public Task(String text, bool add)
        {
            this.text = text;
            this.add = add;
        }

        public String getTextInvolved()
        {
            return this.text;
        }

        public bool isAddingToFile()
        {
            return this.add;
        }

        public void setCompleted()
        {
            this.done = true;
        }

        public bool isTaskDone()
        {
            return this.done;
        }

        public void setOutput(String output)
        {
            this.output = output;
        }

        public String getOutput()
        {
            return this.output;
        }

    }
}