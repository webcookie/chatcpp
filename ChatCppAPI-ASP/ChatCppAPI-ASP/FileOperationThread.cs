﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Threading;
using System.IO;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;

namespace ChatCppAPI_ASP
{
    public class FileOperationThread
    {
        private static Thread fileOperationThread = null;

        private String pathToFile;
        private Queue<Task> threadTasks = new Queue<Task>();
        private Stopwatch lastPingCheck = null;

        public FileOperationThread(String pathToFile)
        {
            this.pathToFile = pathToFile;

            if (fileOperationThread == null)
            {
                fileOperationThread = new Thread(new ThreadStart(run));
                fileOperationThread.Start();
            }
            Console.WriteLine("WARNING : FileOperationThread wont be created more than once.");
        }

        public void run()
        {
            while (true)
            {
                if (threadTasks.Count > 0)
                {
                    Task task = threadTasks.Dequeue();

                    if (!File.Exists(this.pathToFile)) File.CreateText(this.pathToFile).Close();

                    //Read File
                    StreamReader reader = new StreamReader(this.pathToFile);
                    String fileContent = reader.ReadToEnd();
                    reader.Close();

                    if (task.getTextInvolved().Length == 0)
                    {
                        //Task = Read file
                        task.setOutput(fileContent);
                    }
                    else
                    {
                        //Task = Modify file
                        if (task.isAddingToFile())
                        {
                            fileContent += task.getTextInvolved() + "\n";
                        }
                        else
                        {
                            fileContent = fileContent.Replace(task.getTextInvolved() + "\n", "");
                        }

                        //Write new file
                        StreamWriter writer = new StreamWriter(this.pathToFile, false);
                        writer.Write(fileContent);
                        writer.Close();
                    }

                    //Set task completed
                    task.setCompleted();
                }
                /*
                ****COMMENTED THIS WHOLE CODE BECAUSE THE HOSTER DOESNT SUPPORT IT..... ****

                else
                {
                    //Server isn't busy, so why not take the time to ping every server and delete non-working IPs.

                    if (lastPingCheck == null || (lastPingCheck.IsRunning && lastPingCheck.ElapsedMilliseconds * 1000 > 10))
                    {
                        //Avoid NullPointerException
                        if(lastPingCheck == null)
                        {
                            lastPingCheck = new Stopwatch();
                        }
                        //Stop time counter
                        lastPingCheck.Stop();

                        //Read File
                        StreamReader reader = new StreamReader(this.pathToFile);
                        String fileContent = reader.ReadToEnd();
                        reader.Close();

                        //Start thread 
                        new Thread(delegate ()
                        {
                            String[] IPs = fileContent.Split('\n');

                            foreach (String fullIPLine in IPs)
                            {
                                String[] splittedIP = fullIPLine.Split(';');
                                if (splittedIP.Length >= 3)
                                {
                                    String externalIP = splittedIP[0];
                                    String localIP = splittedIP[1];
                                    int port = Convert.ToInt32(splittedIP[2]);

                                    bool localServerWorking = true;
                                    bool externalServerWorking = true;

                                    try
                                    {
                                        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                                        s.Connect(externalIP, port);
                                        s.Disconnect(false);
                                    }
                                    catch (Exception)
                                    {
                                        externalServerWorking = false;
                                    }

                                    //No need to check both if external ip worked
                                    if (!externalServerWorking)
                                    {
                                        try
                                        {
                                            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                                            s.Connect(localIP, port);
                                            s.Disconnect(false);
                                        }
                                        catch (Exception)
                                        {
                                            localServerWorking = false;
                                        }
                                    }
                                    

                                    //If both test failed, server is closed.
                                    if(!localServerWorking && !externalServerWorking)
                                    {
                                        //Delete server from list.
                                        removeServerIP(fullIPLine);
                                    }

                                }
                                //Done cleaning dead IPs. Restard time counter for next check.
                                lastPingCheck = new Stopwatch();
                                lastPingCheck.Start();
                            }
                        }).Start();
                    }
                   */
                System.Threading.Thread.Sleep(10);
            }
        }

        public String getAllServerIPs()
        {
            Task readTask = new Task("", false);
            this.threadTasks.Enqueue(readTask);

            while (!readTask.isTaskDone())
            {
                System.Threading.Thread.Sleep(5);
            }

            return readTask.getOutput();
        }

        public void addServerIP(String ip)
        {
            Task addIPTask = new Task(ip, true);
            this.threadTasks.Enqueue(addIPTask);

            while (!addIPTask.isTaskDone())
            {
                System.Threading.Thread.Sleep(2);
            }
        }

        public void removeServerIP(String ip)
        {
            Task removeIPTask = new Task(ip, false);
            this.threadTasks.Enqueue(removeIPTask);

            while (!removeIPTask.isTaskDone())
            {
                System.Threading.Thread.Sleep(2);
            }
        }

    }
}