#ifndef COMMANDCHRISTMASSONG_H
#define COMMANDCHRISTMASSONG_H

#define COMMAND_CHRISTMAS_SONG "CHRISTMAS_SONG"

#include "Command.hpp"

class CommandChristmasSong : public Command {

public:
    CommandChristmasSong() : Command(COMMAND_CHRISTMAS_SONG){}

    virtual QString getFormattedCommand(){
        return this->prefix;
    }

};

#endif // COMMANDCHRISTMASSONG_H
