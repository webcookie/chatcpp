#ifndef COMMANDCONNECTIONREFUSED_H
#define COMMANDCONNECTIONREFUSED_H

#define COMMAND_CONNECTION_REFUSED "CONNECTIONREFUSED"

#include <QStringList>

#include "Command.hpp"

class CommandConnectionRefused : public Command{

public:
    explicit CommandConnectionRefused(QString reason): Command(COMMAND_CONNECTION_REFUSED){this->reason = reason;}
    QString getReason(){return this->reason;}

    virtual QString getFormattedCommand(){
        return this->prefix + ";" + this->reason;
    }

    static CommandConnectionRefused* parseCommand(QString formattedCommand){
        QStringList split = formattedCommand.split(";");

        if(!formattedCommand.startsWith(COMMAND_CONNECTION_REFUSED)){
            return 0;
        }

        return new CommandConnectionRefused(split.at(1));
    }

private:
    QString reason;

};

#endif // COMMANDCONNECTIONREFUSED_H
