#ifndef COMMAND_H
#define COMMAND_H

#include <QString>
/**
 * @brief Abstract class : Command
 */
class Command {

public:
    explicit Command(QString prefix){this->prefix = prefix;}

    QString getPrefix(){return this->prefix;}
    virtual QString getFormattedCommand() = 0;

protected:
    QString prefix;

};

#endif // COMMAND_H
