#ifndef COMMANDUSERSLIST_H
#define COMMANDUSERSLIST_H

#define COMMAND_USERS_LIST "USERS_LIST"

#include <QStringList>
#include <QString>

#include "Command.hpp"

class CommandUsersList : public Command {

public:
    CommandUsersList(QStringList usersList) : Command(COMMAND_USERS_LIST){
        this->usersList = usersList;
    }

    virtual QString getFormattedCommand(){
        QString formatted = this->prefix;

        for(int i = 0; i < usersList.size(); i++){
            formatted += ";" + usersList.at(i);
        }
        return formatted;
    }

    QStringList getUsersList(){
        return usersList;
    }

    static CommandUsersList* parseCommand(QString formattedCommand){
        QStringList split = formattedCommand.split(";");

        if(!formattedCommand.startsWith(COMMAND_USERS_LIST)){
            return 0;
        }

        QStringList usersList = QStringList();

        for(int i = 1; i < split.size(); i++){
            usersList.append(split.at(i));
        }

        return new CommandUsersList(usersList);
    }

private:
    QStringList usersList;

};

#endif // COMMANDUSERSLIST_H
