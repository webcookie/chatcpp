#ifndef COMMANDGWHOISCLIENT_H
#define COMMANDGWHOISCLIENT_H

#define COMMAND_GWHOIS_CLIENT "GWHOISCLIENT"

#include "Command.hpp"

class CommandGWhoisClient : public Command {

public:
    CommandGWhoisClient() : Command(COMMAND_GWHOIS_CLIENT){}

    virtual QString getFormattedCommand(){
        return this->prefix;
    }

};

#endif // COMMANDGWHOISCLIENT_H
