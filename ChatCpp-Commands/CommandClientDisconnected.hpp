#ifndef COMMANDCLIENTDISCONNECTED_H
#define COMMANDCLIENTDISCONNECTED_H

#define COMMAND_CLIENT_DISCONNECTED "CLIENTDISCONNECTED"

#include <QStringList>

#include "Command.hpp"

class CommandClientDisconnected : public Command {

public:
    CommandClientDisconnected(QString username) : Command(COMMAND_CLIENT_DISCONNECTED){
        this->username = username;
    }

    virtual QString getFormattedCommand(){
        return this->prefix + ";" + this->username;
    }

    QString getUsername(){
        return this->username;
    }

    static CommandClientDisconnected* parseCommand(QString formattedCommand){
        QStringList split = formattedCommand.split(";");

        if(!formattedCommand.startsWith(COMMAND_CLIENT_DISCONNECTED)){
            return 0;
        }

        if(split.size() < 2){
            return 0;
        }

        return new CommandClientDisconnected(split[1]);
    }

private:
    QString username;

};

#endif // COMMANDCLIENTDISCONNECTED_H
