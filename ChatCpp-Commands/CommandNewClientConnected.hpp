#ifndef COMMANDNEWCLIENTCONNECTED_H
#define COMMANDNEWCLIENTCONNECTED_H

#define COMMAND_NEW_CLIENT_CONNECTED "NEWCLIENTCONNECTED"

#include <QStringList>

#include "Command.hpp"

class CommandNewClientConnected : public Command {

public:
    CommandNewClientConnected(QString username) : Command(COMMAND_NEW_CLIENT_CONNECTED){
        this->username = username;
    }

    virtual QString getFormattedCommand(){
        return this->prefix + ";" + this->username;
    }

    QString getUsername(){
        return this->username;
    }

    static CommandNewClientConnected* parseCommand(QString formattedCommand){
        QStringList split = formattedCommand.split(";");

        if(!formattedCommand.startsWith(COMMAND_NEW_CLIENT_CONNECTED)){
            return 0;
        }

        if(split.size() < 2){
            return 0;
        }

        return new CommandNewClientConnected(split[1]);
    }

private:
    QString username;

};

#endif
