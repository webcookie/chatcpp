#ifndef COMMANDMESSAGE_H
#define COMMANDMESSAGE_H

#define COMMAND_MESSAGE "MSG"

#include <QStringList>

#include "Command.hpp"

class CommandMessage : public Command {

public:
    CommandMessage(QString sender, QString message) : Command(COMMAND_MESSAGE){
        this->message = message;
        this->sender = sender;
    }

    virtual QString getFormattedCommand(){
        return this->prefix + ";" + this->sender + ";" + this->message;
    }

    QString getMessage(){
        return this->message;
    }

    QString getSender(){
        return this->sender;
    }

    static CommandMessage* parseCommand(QString formattedCommand){
        QStringList split = formattedCommand.split(";");

        if(!formattedCommand.startsWith(COMMAND_MESSAGE)){
            return 0;
        }

        if(split.length() < 3){
            return 0;
        }

        QString sender = split[1];
        QString message = "";
        for(int i = 2; i < split.size(); i++){
            message += (i > 2 ? ";" : "") + split.at(i); //We want to be able to send ';'
        }

        return new CommandMessage(sender, message);
    }

private:
    QString message;
    QString sender;

};

#endif // COMMANDMESSAGE_H
