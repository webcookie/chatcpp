#ifndef COMMANDMUTE_H
#define COMMANDMUTE_H

#define COMMAND_MUTE "COMMAND_MUTE"

#include <QStringList>
#include <QString>

#include "Command.hpp"

class CommandMute : public Command {

public:
    CommandMute(QString username, bool mute) : Command(COMMAND_MUTE){this->username = username; this->mute = mute;}

    virtual QString getFormattedCommand(){
        return this->prefix + ";" + this->username + ";" + QString::number(this->mute ? 1 : 0);
    }

    QString getUsername(){
        return this->username;
    }

    bool isMute(){
        return this->mute;
    }

    static CommandMute *parseCommand(QString formattedCommand){
        if(!formattedCommand.startsWith(COMMAND_MUTE)){
            return 0;
        }
        QStringList split = formattedCommand.split(";");

        if(split.size() < 3){
            return 0;
        }
        bool conversionSuccess;
        int isMute = split.at(2).toInt(&conversionSuccess);

        if(!conversionSuccess){
            return 0;
        }

        return new CommandMute(split.at(1), (bool)isMute);
    }

private:
    QString username;
    bool mute;
};

#endif // COMMANDMUTE_H
