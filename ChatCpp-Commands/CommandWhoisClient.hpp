#ifndef COMMANDWHOISCLIENT_H
#define COMMANDWHOISCLIENT_H

#define COMMAND_WHOIS_CLIENT "WHOISCLIENT"

#include <QStringList>

#include "Command.hpp"

class CommandWhoisClient : public Command {

public:
    CommandWhoisClient(QString computerName, QString username) : Command(COMMAND_WHOIS_CLIENT){
        this->username = username;
        this->computerName = computerName;
    }

    virtual QString getFormattedCommand(){
        return this->prefix + ";" + this->computerName + ";" + this->username;
    }

    QString getUsername(){
        return this->username;
    }

    QString getComputerName(){
        return this->computerName;
    }

    static CommandWhoisClient* parseCommand(QString formattedCommand){
        QStringList split = formattedCommand.split(";");
        if(split.size() != 3){
            return 0;
        }

        if(split.at(0) != COMMAND_WHOIS_CLIENT){
            return 0;
        }

        QString computerName = split.at(1);
        QString username = split.at(2);

        return new CommandWhoisClient(computerName, username);
    }

private:
    QString username;
    QString computerName;

};


#endif
