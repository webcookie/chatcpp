#-------------------------------------------------
#
# Project created by QtCreator 2014-11-22T08:02:58
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = ChatCpp-Commands
TEMPLATE = lib
CONFIG += staticlib

SOURCES +=

HEADERS += Command.hpp \
    CommandGUsersList.hpp \
    CommandMessage.hpp \
    CommandUsersList.hpp \
    CommandGWhoisClient.hpp \
    CommandWhoisClient.hpp \
    CommandConnectionAccept.hpp \
    CommandConnectionRefused.hpp \
    CommandNewClientConnected.hpp \
    CommandClientDisconnected.hpp \
    CommandChristmasSong.hpp \
    CommandYoutubeMP3.hpp \
    CommandMute.hpp \
    Commands.hpp
unix {
    target.path = /usr/lib
    INSTALLS += target
}

OTHER_FILES +=
